#if !defined(SPACE_AGE_H)
#define SPACE_AGE_H

namespace space_age {
    
    class space_age {
        double earthSecs=31557600;
        double mercurySecs=0.2408467;
        double venusSecs=0.61519726;
        double marsSecs=1.8808158;
        double jupiterSecs;
    };

}  // namespace space_age

#endif // SPACE_AGE_H
